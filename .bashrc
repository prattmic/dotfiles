#
# ~/.bashrc
#

# Edit PATH
PATH=/mnt/share/Programming/embedded/stm32/arm-scratch-cc/bin/:$PATH
export PATH

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1='\[\e[0;32m\]\u@\h\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[0;32m\]\$\[\e[m\] \[\e[m\]'

# Create optical drive symlinks
#for i in cdrom cdrw dvd dvdrw; do ln -s /dev/sr0 /dev/$i; done

# Remap Caps to Esc
xmodmap /home/prattmic/.xmodmap &> /dev/null

# Set bash to vi mode!
set -o vi

# SSH Tunneling through my house
alias sshtunnel='ssh -ND 12345 -v home'
alias ls+='/usr/bin/vendor_perl/ls++'
alias svim='sudo vim'

function mkcd(){
  mkdir $1;
  cd $1;
}

# Vim is the editor
export EDITOR="vim"
